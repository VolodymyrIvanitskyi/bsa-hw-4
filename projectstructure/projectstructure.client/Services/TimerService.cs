﻿using Newtonsoft.Json;
using ProjectStructure.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectStructure.Client.Services
{
    public class TimerService
    {
        private HttpService _httpService;
        public TimerService()
        {
            _httpService = new HttpService();

        }
        public Task<int> MarkRandomTaskWithDelay(int interval)
        {
            Timer _timer = new Timer(interval);
            //Console.WriteLine("start MarkRandomTaskWithDelay");
            var taskCompletionSource = new TaskCompletionSource<int>();

            async void eventHandler(object o, ElapsedEventArgs args)
            {
                //Console.WriteLine("event handler");
                try
                {
                    var task = await GetRandomTask();

                    task.State = TaskStateDTO.Completed;
                    bool isSuccess = await _httpService.PutEntity(Settings.TasksController, task);
                    if (isSuccess)
                    {
                        taskCompletionSource.TrySetResult(task.Id);
                        //Console.WriteLine("success");
                    }
                    else
                    {
                        taskCompletionSource.SetResult(0);
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    _timer.Stop();
                }
            }

            _timer.Start();
            _timer.Elapsed += eventHandler;
            return taskCompletionSource.Task;
        }


        private async Task<TaskDTO> GetRandomTask()
        {
            //Console.WriteLine("start GetrandomTask");
            var tasks = await _httpService.GetEntities<TaskDTO>(Settings.TasksController);
            if (tasks is not null)
            {
                var notCompletedTasks = tasks.Where(task => task.State != TaskStateDTO.Completed).ToList();
                if (!notCompletedTasks.Any())
                {
                    throw new Exception("All the tasks are completed");
                }
                Random random = new Random();
                
                //Console.WriteLine("end GetrandomTask");

                return notCompletedTasks[random.Next(0, notCompletedTasks.Count)];
            }
            else
            {
                throw new Exception("Tasks are empty");
            }
            
        }
    }
}
