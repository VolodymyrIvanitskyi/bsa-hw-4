﻿using Newtonsoft.Json;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.DTO.QueryDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Services
{
    public class QueryService
    {
        private static readonly HttpService _httpService;

        static QueryService()
        {
            _httpService = new HttpService();
        }

        public async Task<Dictionary<int, int>> GetCountTasksByUser(int authorId)
        {
            var result = await _httpService.GetEntity<Dictionary<int, int>>($"{Settings.QueryController}/{Settings.GetCountTasksByUser}/{authorId}");
            return result;
        }

        public async Task<List<TaskDTO>> GetTasksForUser(int userId)
        {
            return await _httpService.GetEntities<TaskDTO>($"{Settings.QueryController}/{Settings.GetTasksForUser}/{userId}") ;
        }

        public async Task<List<TaskDTO>> GetFinishedTasksForUser(int userId)
        {
            return await _httpService.GetEntities <TaskDTO>($"{Settings.QueryController}/{Settings.GetFinishedTasksForUser}/{userId}");

        }

        public async Task<List<OlderTeamDTO>> GetTeamsWhenMembersOlderThan10Years()
        {
            return await _httpService.GetEntities<OlderTeamDTO>($"{Settings.QueryController}/{Settings.GetTeamsWhenMembersOlderThan10Years}");
        }

        public async Task<List<UserTasksDTO>> GetUsersAlphabetically()
        {
            return await _httpService.GetEntities<UserTasksDTO>($"{Settings.QueryController}/{Settings.GetUsersAlphabetically}");
        }

        public async Task<DataFromUserDTO> GetDatafromUser(int userId)
        {
            return await _httpService.GetEntity<DataFromUserDTO>($"{Settings.QueryController}/{Settings.GetDatafromUser}/{userId}");
        }

        public async Task<List<DataFromProjectDTO>> GetDataFromProject()
        {
            return await _httpService.GetEntities<DataFromProjectDTO>($"{Settings.QueryController}/{Settings.GetDataFromProject}");
        }
    }
}
