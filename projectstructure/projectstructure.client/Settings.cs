﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client
{
    public static class Settings
    {
        public const string ServerPath = "http://localhost:60756/api/";

        public const string PostsController = "Post";
        public const string TasksController = "Task";
        public const string UsersController = "User";
        public const string TeamsController = "Team";
        public const string QueryController = "Query";

        public const string GetCountTasksByUser = "GetCountTasksByUser";
        public const string GetTasksForUser = "GetTasksForUser";
        public const string GetFinishedTasksForUser = "GetFinishedTasksForUser";
        public const string GetTeamsWhenMembersOlderThan10Years = "GetTeamsWhenMembersOlderThan10Years";
        public const string GetUsersAlphabetically = "GetUsersAlphabetically";
        public const string GetDatafromUser = "GetDatafromUser";
        public const string GetDataFromProject = "GetDataFromProject";
    }
}
