﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.DTO.QueryDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private readonly IQueryService _queryService;
        public QueryController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        [HttpGet("GetCountTasksByUser/{id}")]
        public async Task<ActionResult<Dictionary<int,int>>> GetCountTasksByUser(int id)
        {
            try
            {
                var tasks = await _queryService.GetCountTasksByUser(id);//ConfigureAwait(false)
                return new JsonResult(tasks);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetTasksForUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksForUser(int id)
        {
            try
            {
                var tasks = await _queryService.GetTasksForUser(id);
                return new JsonResult(tasks);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetFinishedTasksForUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetFinishedTasksForUser(int id)
        {
            try
            {
                var finishedTasks = await _queryService.GetFinishedTasksForUser(id);
                return new JsonResult(finishedTasks);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetTeamsWhenMembersOlderThan10Years")]
        public async Task<ActionResult<IEnumerable<OlderTeamDTO>>> GetTeamsWhenMembersOlderThan10Years()
        {
            try
            {
                var teams = await _queryService.GetTeamsWhenMembersOlderThan10Years();
                return new JsonResult(teams);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetUsersAlphabetically")]
        public async Task<ActionResult<IEnumerable<UserTasksDTO>>> GetUsersAlphabetically()
        {
            try
            {
                var users = await _queryService.GetUsersAlphabetically();
                return new JsonResult(users);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetDatafromUser/{id}")]
        public async Task<ActionResult<DataFromUserDTO>> GetDatafromUser(int id)
        {
            try
            {
                var data = await _queryService.GetDatafromUser(id);
                return new JsonResult(data);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("GetDataFromProject")]
        public async Task<ActionResult<IEnumerable<DataFromProjectDTO>>> GetDataFromProject()
        {
            try
            {
                var data = await _queryService.GetDataFromProject();
                return new JsonResult(data);
            }
            catch (NotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }

    }
}
