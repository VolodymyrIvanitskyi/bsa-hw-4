﻿using AutoMapper;
using ProjectStructure.Common.DTO;

namespace ProjectStructure.BL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<DAL.Models.Task, TaskDTO>();
            CreateMap<TaskDTO, DAL.Models.Task>();
        }
    }
}
