﻿using AutoMapper;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
