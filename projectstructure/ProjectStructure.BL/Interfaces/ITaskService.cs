﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetAllTasks();
        Task<TaskDTO> GetTaskById(int id);
        Task CreateTask(TaskDTO task);
        Task UpdateTask(TaskDTO task);
        Task DeleteTask(int taskId);
    }
}
