﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetAllProjects();
        Task<ProjectDTO> GetProjectById(int id);
        Task CreateProject(ProjectDTO project);
        Task UpdateProject(ProjectDTO project);
        Task DeleteProject(int projectId);
    }
}
