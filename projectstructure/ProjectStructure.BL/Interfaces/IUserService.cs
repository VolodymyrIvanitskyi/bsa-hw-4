﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetAllUsers();
        Task<UserDTO> GetUserById(int id);
        Task CreateUser(UserDTO user);
        Task UpdateUser(UserDTO user);
        Task DeleteUser(int userId);
    }
}
