﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetAllTeams();
        Task<TeamDTO> GetTeamById(int id);
        Task CreateTeam(TeamDTO team);
        Task UpdateTeam(TeamDTO team);
        Task DeleteTeam(int teamId);
    }
}
