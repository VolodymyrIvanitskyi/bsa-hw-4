﻿using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Team> _teamRepository;

        public TeamService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _teamRepository = _unitOfWork.TeamRepository;
        }
        public async System.Threading.Tasks.Task CreateTeam(TeamDTO teamDTO)
        {
            if (await _teamRepository.Get(teamDTO.Id) is not null)
                throw new Exception($"Team with id {teamDTO.Id} already exists");

            var teamEntity = _mapper.Map<Team>(teamDTO);
            await _teamRepository.Create(teamEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteTeam(int teamId)
        {
            if (await _teamRepository.Get(teamId) is null)
                throw new NotFoundException("Team", teamId);

            await _teamRepository.Delete(teamId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            var allTeams = await _teamRepository.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(allTeams);
        }

        public async System.Threading.Tasks.Task<TeamDTO> GetTeamById(int teamId)
        {
            var teamEntity = await _teamRepository.Get(teamId);
            if(teamEntity is null)
                throw new NotFoundException("Team", teamId);

            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public async System.Threading.Tasks.Task UpdateTeam(TeamDTO teamDTO)
        {
            if (await _teamRepository.Get(teamDTO.Id) is null)
                throw new NotFoundException("Team", teamDTO.Id);

            var teamEntity = await _teamRepository.Get(teamDTO.Id);
            teamEntity = _mapper.Map(teamDTO, teamEntity);
            _teamRepository.Update(teamEntity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
