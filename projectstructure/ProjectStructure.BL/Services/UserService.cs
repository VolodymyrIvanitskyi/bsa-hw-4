﻿using AutoMapper;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<User> _userRepository;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.UserRepository;
        }

        public async System.Threading.Tasks.Task CreateUser(UserDTO userDTO)
        {
            if (await _userRepository.Get(userDTO.Id) is not null)
                throw new Exception($"User with id {userDTO.Id} already exists");

            var userEntity = _mapper.Map<User>(userDTO);
            await _userRepository.Create(userEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteUser(int userId)
        {
            if (await _userRepository.Get(userId) is null)
                throw new NotFoundException("User", userId);

            await _userRepository.Delete(userId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            var allUsers = await _userRepository.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(allUsers);
        }

        public async System.Threading.Tasks.Task<UserDTO> GetUserById(int userId)
        {
            var userEntity = await _userRepository.Get(userId);
            if (userEntity is null)
                throw new NotFoundException("User", userId);

            return _mapper.Map<UserDTO>(userEntity);
        }

        public async System.Threading.Tasks.Task UpdateUser(UserDTO userDTO)
        {
            if (await _userRepository.Get(userDTO.Id) is null)
                throw new NotFoundException("User", userDTO.Id);

            var userEntity = await _userRepository.Get(userDTO.Id);
            userEntity = _mapper.Map(userDTO, userEntity);
            _userRepository.Update(userEntity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
