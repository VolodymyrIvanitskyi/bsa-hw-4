﻿using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.DAL.Models;
using System;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Task> _taskRepository;

        public TaskService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _taskRepository = _unitOfWork.TaskRepository;
        }
        public async System.Threading.Tasks.Task CreateTask(TaskDTO taskDTO)
        {
            if (await _taskRepository.Get(taskDTO.Id) is not null)
                throw new Exception($"Task with id {taskDTO.Id} already exists");

            var taskEntity = _mapper.Map<Task>(taskDTO);
            await _taskRepository.Create(taskEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteTask(int taskId)
        {
            if (await _taskRepository.Get(taskId) is null)
                throw new NotFoundException("Task", taskId);

            await _taskRepository.Delete(taskId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task<IEnumerable<TaskDTO>> GetAllTasks()
        {
            var alltasks = await _taskRepository.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(alltasks);
        }

        public async System.Threading.Tasks.Task<TaskDTO> GetTaskById(int taskId)
        {
            var taskEntity = await _taskRepository.Get(taskId);
            if(taskEntity is null)
                throw new NotFoundException("Task", taskId);

            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public async System.Threading.Tasks.Task UpdateTask(TaskDTO taskDTO)
        {
            if (await _taskRepository.Get(taskDTO.Id) is null)
                throw new NotFoundException("Task", taskDTO.Id);

            var taskEntity = await _taskRepository.Get(taskDTO.Id);
            taskEntity = _mapper.Map(taskDTO, taskEntity);
            _taskRepository.Update(taskEntity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
