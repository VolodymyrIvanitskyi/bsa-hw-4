﻿
namespace ProjectStructure.Common.DTO.QueryDTO
{
    public class DataFromProjectDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO TheLongestTask { get; set; }
        public TaskDTO TheShortestTask { get; set; }
        public int? CountOfUsers { get; set; }

    }
}
