﻿using ProjectStructure.DAL.Models;

namespace ProjectStructure.DAL.UnitOfWork.Interfaces
{
    public interface IUnitOfWork
    {
        public IRepository<Project> ProjectRepository { get; }
        public IRepository<Team> TeamRepository { get; }
        public IRepository<Models.Task> TaskRepository { get; }
        public IRepository<User> UserRepository { get; }
        public void SaveChanges();
        public System.Threading.Tasks.Task<int> SaveChangesAsync();
    }
}
