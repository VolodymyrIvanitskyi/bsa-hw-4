﻿using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using ProjectStructure.DAL.UnitOfWork.Reposity;

namespace ProjectStructure.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectDBContext _dBContext;

        private IRepository<Project> _projectRepository;
        private IRepository<User> _userRepository;
        private IRepository<Team> _teamRepository;
        private IRepository<Task> _taskRepository;

        public UnitOfWork(ProjectDBContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IRepository<Project> ProjectRepository
        {
            get
            {
                return _projectRepository ??
                    (_projectRepository = new ProjectRepository(_dBContext));
            }
        }

        public IRepository<Team> TeamRepository 
        {
            get 
            {
                return _teamRepository ??
                    (_teamRepository = new TeamRepository(_dBContext));
            }
        }

        public IRepository<Task> TaskRepository
        {
            get
            {
                return _taskRepository ??
                    (_taskRepository = new TaskRepository(_dBContext));
            }
        }

        public IRepository<User> UserRepository
        {
            get
            {
                return _userRepository ??
                    (_userRepository = new UserRepository(_dBContext));
            }
        }

        public void SaveChanges()
        {
            _dBContext.SaveChanges();
        }

        public async System.Threading.Tasks.Task<int> SaveChangesAsync()
        {
            return await _dBContext.SaveChangesAsync();
        }
    }
}
