﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class ProjectRepository : IRepository<Project>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public ProjectRepository(ProjectDBContext dBContext)
        {
			_dBContext = dBContext;
        }
		public async System.Threading.Tasks.Task Create(Project project)
		{
			 await _dBContext.Projects.AddAsync(project);
		}

		public async System.Threading.Tasks.Task Delete(int id)
		{
			var project = await _dBContext.Projects.FindAsync(id);
			_dBContext.Projects.Remove(project);
		}

		public async Task<Project> Get(int id)
		{
			var result = await _dBContext.Projects.FindAsync(id);
			return result;
		}

		public async Task<IEnumerable<Project>> GetAll()
		{
			return await _dBContext.Projects.ToListAsync();
		}

		public void Update(Project project)
		{
			_dBContext.Entry(project).State = EntityState.Modified;
		}

		public void Save()
        {
			_dBContext.SaveChanges();
        }

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
