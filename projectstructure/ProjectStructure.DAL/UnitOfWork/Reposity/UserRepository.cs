﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class UserRepository : IRepository<User>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public UserRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public async System.Threading.Tasks.Task Create(User user)
		{
			await _dBContext.Users.AddAsync(user);
		}

		public async System.Threading.Tasks.Task Delete(int id)
        {
            var user = await _dBContext.Users.FindAsync(id);
            _dBContext.Users.Remove(user);
        }

		public async Task<User> Get(int id)
		{
			return await _dBContext.Users.FindAsync(id);
		}

		public async Task<IEnumerable<User>> GetAll()
		{
			return await _dBContext.Users.ToListAsync();
		}

		public void Update(User user)
		{
			_dBContext.Entry(user).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
