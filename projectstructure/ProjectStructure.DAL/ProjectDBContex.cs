﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL
{
    public class ProjectDBContext : DbContext
    {
        public ProjectDBContext(DbContextOptions<ProjectDBContext> options)
            : base(options)
        { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>()
			{
				new Project()
				{
					Id = 1,
					AuthorId = 1,
					TeamId = 1,
					Name = "backing up Handcrafted Fresh Shoes challenge",
					Description = "Et doloribus et temporibus.",
					Deadline = DateTime.Parse("2021-09-12T19:17:47.2335223+00:00"),
					CreatedAt =  DateTime.Parse("2021-08-25T17:49:50.4518054+00:00")
				},
				new Project()
				{
					Id = 2,
					AuthorId = 2,
					TeamId = 1,
					Name = "Village",
					Description = "Non voluptatem voluptas libero.",
					Deadline = DateTime.Parse("2021-07-24T12:07:31.9357846+00:00"),
					CreatedAt =  DateTime.Parse("2021-01-30T14:38:53.8838745+00:00")
				},
				new Project()
				{
					Id = 3,
					AuthorId = 3,
					TeamId = 1,
					Name = "Dam",
					Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
					Deadline = DateTime.Parse("2021-07-21T08:32:51.3354654+00:00"),
					CreatedAt =  DateTime.Parse("2020-03-15T20:33:15.6731141+00:00")
				},
				new Project()
				{
					Id = 4,
					AuthorId = 4,
					TeamId = 2,
					Name = "iterate project",
					Description = "Soluta non sed assumenda.",
					Deadline = DateTime.Parse("2022-06-25T11:25:00.7111264+00:00"),
					CreatedAt =  DateTime.Parse("2021-07-03T03:39:01.9978679+00:00")
				},
				new Project()
				{
					Id = 5,
					AuthorId = 5,
					TeamId = 2,
					Name = "Libyan Dinar Netherlands Antilles",
					Description = "Voluptatibus error ut id libero quam natus molestias natus.",
					Deadline = DateTime.Parse("2022-04-14T02:53:20.8003038+00:00"),
					CreatedAt =  DateTime.Parse("2021-01-15T19:47:35.9335401+00:00")
				},
				new Project()
				{
					Id = 6,
					AuthorId = 6,
					TeamId = 3,
					Name = "New Jersey capacitor program",
					Description = "Quas fuga qui eaque et corporis.",
					Deadline = DateTime.Parse("2022-09-23T03:11:42.1994133+00:00"),
					CreatedAt =  DateTime.Parse("2022-01-22T16:51:40.1965166+00:00")
				}
			};

			var tasks = new List<Task>()
			{
				new Task()
				{
					Id = 2,
					ProjectId = 2,
					PerformerId = 2,
					Name = "product Direct utilize",
					Description = "Eum a eum.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2022-01-15T15:06:52.0600666+00:00"),
					FinishedAt = null
				},
				new Task()
				{
					Id = 3,
					ProjectId = 3,
					PerformerId = 3,
					Name = "bypass",
					Description = "Sint voluptatem quas.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2021-08-16T06:13:44.5773845+00:00"),
					FinishedAt = DateTime.Parse("2021-09-09T06:34:47.460216+00:00")
				},
				new Task()
				{
					Id = 4,
					ProjectId = 2,
					PerformerId = 4,
					Name = "withdrawal contextually-based",
					Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2022-01-19T00:58:34.8045103+00:00"),
					FinishedAt = null
				},
				new Task()
				{
					Id = 5,
					ProjectId = 2,
					PerformerId = 3,
					Name = "mobile Organized",
					Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2021-12-15T06:03:48.0732466+00:00"),
					FinishedAt = null
				},
				new Task()
				{
					Id = 6,
					ProjectId = 2,
					PerformerId = 2,
					Name = "world-class Circles",
					Description = "Reiciendis iusto rerum non et aut eaque.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2021-10-21T14:56:53.8117818+00:00"),
					FinishedAt = DateTime.Parse("2021-12-21T14:56:53.8117818+00:00")
				},
				new Task()
				{
					Id = 7,
					ProjectId = 3,
					PerformerId = 4,
					Name = "Automotive & Tools transitional bifurcated",
					Description = "Et rerum ad.",
					State = (TaskState)2,
					CreatedAt = DateTime.Parse("2021-11-10T16:21:12.0886153+00:00"),
					FinishedAt = DateTime.Parse("2021-12-10T16:21:12.0886153+00:00")
				}
			};

			var teams = new List<Team>()
			{
				new Team()
				{
					Id = 1,
					Name = "Durgan Group",
					CreatedAt = DateTime.Parse("2021-03-31T02:29:28.3740504+00:00")

				},
				new Team()
				{
					Id = 2,
					Name = "Kassulke LLC",
					CreatedAt = DateTime.Parse("2021-02-21T15:47:30.3797852+00:00")

				},
				new Team()
				{
					Id = 3,
					Name = "Harris LLC",
					CreatedAt = DateTime.Parse("2020-08-28T08:18:46.4160342+00:00")

				},
				new Team()
				{
					Id = 4,
					Name = "Mitchell Inc",
					CreatedAt = DateTime.Parse("2021-04-03T09:58:33.0178179+00:00")
				},
				new Team()
				{
					Id = 5,
					Name = "Smitham Group",
					CreatedAt = DateTime.Parse("2019-10-05T07:57:02.8427653+00:00")
				},
				new Team()
				{
					Id = 6,
					Name = "Kutch - Roberts",
					CreatedAt = DateTime.Parse("2020-10-31T05:05:15.1076578+00:00")

				},
				new Team()
				{
					Id = 7,
					Name = "Parisian Group",
					CreatedAt = DateTime.Parse("2021-07-17T01:34:55.0917082+00:00")
				}
			};

			var users = new List<User>()
			{
				new User()
				{
					Id = 1,
					TeamId = 1,
					FirstName = "Theresa",
					LastName = "Gottlieb",
					Email = "Theresa_Gottlieb66@yahoo.com",
					RegisteredAt = DateTime.Parse("2019-12-04T16:52:04.3727619+00:00"),
					BirthDay = DateTime.Parse("1992-09-27T20:27:03.5236015+00:00"),

				},
				new User()
				{
					Id=2,
					TeamId = 1,
					FirstName = "Brandy",
					LastName = "Witting",
					Email = "Brandy.Witting@gmail.com",
					RegisteredAt = DateTime.Parse("2019-01-10T02:51:56.7148896+00:00"),
					BirthDay = DateTime.Parse("2007-01-01T05:10:18.898869+00:00"),


				},
				new User()
				{
					Id=3,
					TeamId = 1,
					FirstName = "Theresa",
					LastName = "Ebert",
					Email = "Theresa82@hotmail.com",
					RegisteredAt = DateTime.Parse("2021-05-14T02:37:44.4486766+00:00"),
					BirthDay = DateTime.Parse("1980-02-20T16:32:12.6358667+00:00")
				},
				new User()
				{
					Id=4,
					TeamId = 2,
					FirstName = "Alfredo",
					LastName = "Simonis",
					Email = "Alfredo_Simonis@yahoo.com",
					RegisteredAt = DateTime.Parse("2020-10-14T16:40:52.2772028+00:00"),
					BirthDay = DateTime.Parse("1954-04-09T05:04:50.4709098+00:00")

				},
				new User()
				{
					Id=5,
					TeamId = 2,
					FirstName = "Joanna",
					LastName = "Botsford",
					Email = "Joanna25@hotmail.com",
					RegisteredAt = DateTime.Parse("2021-05-14T18:02:44.0995821+00:00"),
					BirthDay = DateTime.Parse("2009-05-12T02:09:16.2373321+00:00")
				},
				new User()
				{
					Id=6,
					TeamId = 2,
					FirstName = "Ann",
					LastName = "Langworth",
					Email = "Ann.Langworth@hotmail.com",
					RegisteredAt = DateTime.Parse("2019-08-09T05:47:42.3699036+00:00"),
					BirthDay = DateTime.Parse("1962-02-26T08:08:59.071182+00:00")
				},
				new User()
				{
					Id=7,
					TeamId = 3,
					FirstName = "Christie",
					LastName = "Gusikowski",
					Email = "Christie.Gusikowski@hotmail.com",
					RegisteredAt = DateTime.Parse("2021-10-07T07:41:43.2460699+00:00"),
					BirthDay = DateTime.Parse("1981-01-10T11:38:30.5290222+00:00")
				}
			};


			modelBuilder.Entity<Project>().HasData(projects);
			modelBuilder.Entity<Team>().HasData(teams);
			modelBuilder.Entity<Task>().HasData(tasks);
			modelBuilder.Entity<User>().HasData(users);

			base.OnModelCreating(modelBuilder);
		}
    }
}
