﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class RenameCollumnBirthdayToBirthDay : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Birthday",
                table: "Users",
                newName: "BirthDay");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BirthDay",
                table: "Users",
                newName: "Birthday");
        }
    }
}
